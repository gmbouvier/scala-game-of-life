# Scala GOL

To get this running you'll probably need JDK8, Scala, and sbt.

From there a simple
```
sbt clean compile assembly && java -jar ./target/scala-2.13/hello-world-assembly-1.0.jar 500 $(tput cols) $(tput lines)
```
in the target directory should get it running.

## todo

- RLE importer
- Wrap-around support
