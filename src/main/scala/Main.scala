object Main {
  type GolBoard = Array[Array[Int]]

  def printGolBoard(board: GolBoard): Unit = {
    // Calculating the boardString ahead of printing ensures that only
    //  print latency occurs between frames
    val boardString = board.map {
      row => { row.map { x => if (x == 1) '█' else ' ' }.mkString }
    }.mkString("\n")

    // These print statements clear the terminal
    print("\u001b[2J"); print("\u001b[H")
    println(boardString)
  }

  // A semaphore is the most basic Game of Life oscillator
  def addSemaphore(board: GolBoard, x: Int = 1, y: Int = 1): GolBoard = {
    (y-1 to y+1).foreach { board(x)(_) = 1 }
    board
  }

  // A glider will travel down and right (relative to how the board is printed)
  def addGlider(board: GolBoard, x: Int = 2, y: Int = 2): GolBoard = {
    (y-1 to y+1).foreach { board(x+1)(_) = 1 }
    board(x-1)(y) = 1
    board(x)(y+1) = 1
    board
  }

  // This will fill the board with a random initial condition
  def makeRandom(board: GolBoard): GolBoard = {
    val random = new scala.util.Random
    board.map { row => { row.map { _ => random.nextInt(2) } } }
  }

  def nextTurn(board: GolBoard): GolBoard = {
    // Slice the neighbouring section of the Board
    //  No out of range exceptions happen with `slice`, and since we
    //  adding `Int`s to determine neighbours, all "off-board" positions
    //  are inherently counted as `0`
    def neighbourhood(x: Int, y: Int): GolBoard = {
      board.slice(x-1, x+2).map { _.slice(y-1, y+2) }
    }

    // Returns the count of neighbours excluding the considered position
    def neighbours(x: Int, y: Int): Int = {
      neighbourhood(x, y).flatten[Int].sum - board(x)(y)
    }

    // All the Game of Life rules are encapsulated here
    // Simply returns a Boolean as to whether the cell should be alive
    //  in the next game state
    def aliveNextTurn(cell: Int, neighbours: Int): Boolean = {
      (neighbours == 3) || (neighbours + cell == 3)
    }

    // zipWithIndex is not the most intuitive way to access an index
    //  while mapping, but it works well enough for now
    board.zipWithIndex.map {
      case (row, x) =>
        row.zipWithIndex.map {
          case (cell, y) => {
            if (aliveNextTurn(cell, neighbours(x, y))) 1 else 0
          }
        }
    }
  }

  def main(args: Array[String]) = {
    var (turns, width, height, wait) = (50, 50, 50, 100)
    if (args.length > 0) { turns = args(0).toInt }
    if (args.length > 1) { width = args(1).toInt }
    if (args.length > 2) { height = args(2).toInt }
    if (args.length > 3) { wait = args(3).toInt }

    var board = makeRandom(Array.ofDim[Int](height, width))
    0.to(turns).foreach {
      _ => {
        printGolBoard(board)
        board = nextTurn(board)
        Thread.sleep(wait)
      }
    }
  }
}
